/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestingResult;
import java.util.ArrayList;
/**
 *
 * @author aman
 */
public class TestTag {
    ArrayList<String> topTagname=new ArrayList<>(); //storing recomended top tags
    int row=7;
    int [] topCorrectTagsCount=new int [row]; //stores |tagsicorrect|
     int []topTagsCount=new int[row]; //stores |tagsitopK|

    int tagsCorrect=20; //choosen 2000 test s/w object for each tag so tags correct is 2000; stores |tagsi correct| for each tag
    ArrayList<String> tagsList=new ArrayList<>();//tags list
    int swobjTotal=140;
    public TestTag()
    {   
    }
    public TestTag(ArrayList str)
    {
        topTagname=str;
        tagsList.add("Java");
        tagsList.add("PHP");
        tagsList.add("C#");
        tagsList.add("HTML");
        tagsList.add("IOS");
        tagsList.add("XML");
        tagsList.add("JavaScript");
        
        
        
    }
    public void setTopTagsCount()
    {
        for(int i=0;i<row;i++)
        {
            for(int j=0;j<swobjTotal;j++)
            {
               if(tagsList.get(i).equals(topTagname.get(j))){
                   
                   topTagsCount[i]++; //counting TopTags for each tags
               } 
               
                
            }
            
            System.out.println("TopTagsCount"+i+":"+topTagsCount[i]);
        }
        
        
        
      
    }
    
    
    public void setTopTagsCorrectCount()
    {
         for(int i=0;i<row;i++)
        {
            topCorrectTagsCount[i]=0;
            for(int j=20*i;j<20+(20*i);j++)
            {
              if(tagsList.get(i).equals(topTagname.get(j))){
                   
                   topCorrectTagsCount[i]++; //counting topCorrectTags for each tag
               } 
            }
            System.out.println("topCorrectTagsCount"+i+":"+topCorrectTagsCount[i]);
        }
        
    }    
    
    public double calculatePrecision()
    {
        double precision=0;
        for(int i=0;i<row;i++)
        {
            
            precision+=topCorrectTagsCount[i]/(double)topTagsCount[i];
        }
        return (precision/row);
        
    }
            
    public double calculateRecall()
    {
        double recall=0;
        for(int i=0;i<row;i++)
        {
            
       recall+=topCorrectTagsCount[i]/(double)tagsCorrect;
          
            //System.out.println("recall="+recall);
        }
        return (recall/row);
        
        
        
        
    }
    public void displayTagsList()
    {
        for(int i=0;i<topTagname.size();i++)
        {
            System.out.println("taglist"+topTagname.get(i));
            
            
        }
        
    }
            
            
    
}
