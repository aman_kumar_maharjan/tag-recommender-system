/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestingResult;

import bic.BIC;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

/**
 *
 * @author aman
 */
public class TestResult {

   
   
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        
         FileInputStream file = new FileInputStream(new File("test.xls"));
            HSSFWorkbook wb = new HSSFWorkbook(file);
            CreationHelper createHelper = wb.getCreationHelper();
            Sheet sheet = wb.getSheet("sheet1");
            ArrayList<String> topTagList=new ArrayList<>(); // for storing  recommended tags for each testing s/w obj
           
            for(int i=0;i<140;i++)
            {
              Row row = sheet.getRow(i);
             String swObj=row.getCell(3).toString(); 
                System.out.println(i+swObj);
             //reading every testing sw object and generating corrensponding tag
              BIC test= new BIC(swObj);
             
              
              
              topTagList.add(test.getTopTag()); //getting recommended tag for each testing swobj
            }
            System.out.println("taglist size="+topTagList.size());
            
           TestTag test=new TestTag(topTagList);
           test.displayTagsList();
       test.setTopTagsCorrectCount();
       test.setTopTagsCount();
           
           
          double precision=test.calculatePrecision();
         double recall=test.calculateRecall();
         System.out.println(" precision="+precision);
      
        System.out.println("recall="+recall);
           
           
            
            
            
       
       
    }
    
    
    
    
    
    
}
