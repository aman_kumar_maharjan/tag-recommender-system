package bic;

import dao.DAO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import preprocessor.PreProcessor;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class BIC {
   
    
    
    
    

    public static void main(String[] args) throws IOException {
        
        String swObj;
        ArrayList<String> wordList = new ArrayList<>();
        int index[] = null;    //an array to store the index of words found in database
      System.out.println("enter your question");
      /// swObj = JOptionPane.showInputDialog(null, "Enter your question");
       BufferedReader bfr= new BufferedReader(new InputStreamReader(System.in));
        swObj=bfr.readLine().toString();

        System.out.println("input="+swObj);

//        perform pre-processing on the given software object and store in wordList
        wordList = new PreProcessor(swObj).process();
        if(wordList.isEmpty())
        {
            System.out.println("Noun not found....No recommendations.... add more specific content");
            
        }

//        create Database Access Object
        DAO dao = new DAO();
        try {
//            connect to "entagrec" database connection as "localhost"
            dao.con("localhost", "entagrec");

//            search database for the existence of words
            if (dao.search(wordList)) {
                System.out.println("Search successful");
//          index[] stores the index of words found in database
                index = dao.getIndexArray();/////////////////////////////////////////////////////////////
            }else{
                System.out.println("No recommendations.... add more specific content");
               System.exit(0);
            }
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        } catch (ClassNotFoundException ex) {
            System.out.println("Error: " + ex);
        } finally {
            try {
                dao.closeCon();
            } catch (SQLException ex) {
                System.out.println("Error:Null statement " + ex);
            }
        }
         System.out.println("====================================================");

        TopicTermDistribution ttd = new TopicTermDistribution();
        ttd.setTopicTermValues(index);
        ttd.printTopicTermDistribution();

//        compute P(tags|wordList) using Bayesian Rule
        String topictermvalues[][] = ttd.getTopicTermValues();
        Tag tags[] = new Tag[7];
        tags[0] = new Tag("Java", 70168.0);
        tags[1] = new Tag("PHP", 60144.0);
        tags[2] = new Tag("C#", 66015.0);
        tags[3] = new Tag("HTML",67079.0);
        tags[4] = new Tag("IOS", 65820.0);
        tags[5] = new Tag("XML", 56316.0);
        tags[6] = new Tag("JavaScript", 56316.0);

        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < index.length; j++) {
                tags[i].prob += (Double.parseDouble(topictermvalues[i][j])) / tags[i].total;
            }
            tags[i].prob /= 7.0;
            System.out.println(tags[i].prob);
           // System.out.println(tags[i].prob);
        }
        System.out.println(Tag.getHighestProbTag(tags).prob);
        double threshold =0.000152 ;// threshold value taken for frequency greater than 50 taking average total 62000
        if(Tag.getHighestProbTag(tags).prob>threshold){
        System.out.println("====================================================");
        System.out.println("Recommended Tag: " + Tag.getHighestProbTag(tags).name);
        }
        else{
            System.out.println("No recommendations.... add more specific content to programming terms like");
            System.out.println("i.e. "+swObj+" in/for/into/......"+"java/php/android.....etc.");
        }
    }
    
    }
    

